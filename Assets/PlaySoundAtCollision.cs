﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundAtCollision : MonoBehaviour
{
	public float ballSpeed = 7f;

    void OnCollisionEnter (Collision col)
    {
        GetComponent<AudioSource> ().Play ();
		//Vector3 velocityVector = new Vector3(Random.Range(-0.5f, 0.5f), Random.Range(0, 1.0f), Random.Range(-0.5f, 0.5f));
		//velocityVector.Normalize();
		//GetComponent<Rigidbody>().velocity = velocityVector * ballSpeed;
	}
}
