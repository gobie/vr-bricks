﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BallEvents : MonoBehaviour {

	public ParticleSystem particle;
	public UnityEvent OnWallCollision;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//if (Input.GetKeyDown(KeyCode.L))
		//{
		//	OnWallCollision.Invoke();
		//}
	}

	private void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "CubeBig")
		{
			OnWallCollision.Invoke();
			Debug.Log(collision.gameObject.name);
		}
	}

	public void PlayParticle()
	{
		particle.Play();
	}
}
