﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnGameFinish : MonoBehaviour
{
    void OnEnable ()
    {
        GameManagerInterface.OnGameStop += DestroyThis;
    }

    void OnDisable ()
    {
        GameManagerInterface.OnGameStop -= DestroyThis;
    }

    void DestroyThis (float time, float score)
    {
        Destroy (this.gameObject);
    }
}
