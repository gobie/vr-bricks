﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStartStopSound : MonoBehaviour
{
    public AudioClip GameStartSound;
    public AudioClip GameStopSound;

    void OnEnable ()
    {
        GameManagerInterface.OnGameStart += GameStarted;
        GameManagerInterface.OnGameStart += GameStopped;
    }

    void OnDisable ()
    {
        GameManagerInterface.OnGameStart -= GameStarted;
        GameManagerInterface.OnGameStart -= GameStopped;
    }

    void GameStarted ()
    {
        AudioSource.PlayClipAtPoint (GameStartSound, this.transform.position);
    }

    void GameStopped ()
    {
        AudioSource.PlayClipAtPoint (GameStopSound, this.transform.position);
    }
}
