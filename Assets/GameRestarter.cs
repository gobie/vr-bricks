﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRestarter : MonoBehaviour {

	public GameObject ball;
	public Transform ballSpawnPoint;

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.R))
		{
			GameRestart();
		}	
	}

	public void GameRestart()
	{
		Instantiate(ball, ballSpawnPoint.position, Quaternion.identity);
	}
}
