﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpawner : MonoBehaviour
{
    public int NumberOfBalls = 3;
    public GameObject BallPrefab;

	public static BallSpawner instance;  

	public int ActualNumberOfBalls = 0;

	void Awake ()
	{
		if (instance == null) {
			instance = this;
		}
	}

    void Start ()
    {
        Debug.Assert (BallPrefab != null, "Need ball prefab for ball spawner");
    }

	void Update () {
		if (Input.GetKeyDown(KeyCode.R))
		{
			if (Object.FindObjectOfType<GameManagerInterface> ().GameIsRunning) {
				SpawnBall ();
			}
		}	
	}

    void OnEnable ()
    {
        GameManagerInterface.OnGameStart += SpawnBalls;
    }

    void OnDisable ()
    {
        GameManagerInterface.OnGameStart -= SpawnBalls;
    }

    void SpawnBalls ()
    {
        for (int i = 0; i < NumberOfBalls; i++) {
			SpawnBall ();
        }
    }


	public void SpawnBall()
	{
		Instantiate (BallPrefab, this.transform.position, Quaternion.identity);
		ActualNumberOfBalls++;
	}

	public void DecreaseBalls()
	{
		ActualNumberOfBalls--;
		if (ActualNumberOfBalls <= 0) {
			Object.FindObjectOfType<GameManagerInterface> ().StopGame ();
		}
	}

}
