﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickControl : MonoBehaviour
{
    private bool isBlocking = true;
    private Collider brickCollider;
    private float brickTransparencyWhenHit = 0.5f;
	public AudioClip BrickExplosionSFX;

    [SerializeField]
    private BrickSetController brickSetController;

    // Use this for initialization
    void Awake ()
    {
        brickCollider = GetComponent<Collider> ();
        Debug.Assert (brickCollider != null, "Brick Collider not found");
    }
	
    // Update is called once per frame

    /*
	void Update () {
		
	}
	*/


    void OnTriggerEnter (Collider coll)
    {
        //Debug.LogWarning ("OnTriggerEnter ");

        if (coll.gameObject.tag == "Ball") {
            handleBallHits ();
        }
    }

    void OnCollisionEnter (Collision coll)
    {
        //Debug.LogWarning ("OnTriggerEnter ");

        if (coll.gameObject.tag == "Ball") {
            handleBallHits ();
        }
    }

    void handleBallHits ()
    {
        if (!isBlocking) {
            return;
        }
        // after first hit brick is turned to trigger so it not bouces back the ball
        brickCollider.isTrigger = true;
        isBlocking = false;
        MakeBrickTransparent ();

        if (brickSetController != null) {
            brickSetController.DecreaseNumBricks ();
        }

		if (BrickExplosionSFX != null) {
			AudioSource.PlayClipAtPoint (BrickExplosionSFX, transform.position);
		}

    }

    public void SetBrickTransparencyWhenHit (float f)
    {
        this.brickTransparencyWhenHit = f;
    }

    public void SetBrickSetController (BrickSetController b)
    {
        this.brickSetController = b;
    }

    private void MakeBrickTransparent ()
    {

        Renderer[] allRenderers = gameObject.transform.parent.gameObject.GetComponentsInChildren<Renderer> ();
        if (allRenderers.Length > 0) {
            Renderer brickRenderer = allRenderers [0];
            Material newMat = Instantiate (brickRenderer.material);

            float r = newMat.color.r;
            float g = newMat.color.g;
            float b = newMat.color.b;
            float a = brickTransparencyWhenHit;
            Color c = new Color (r, g, b, a);
            foreach (Renderer renderer in allRenderers) {
                renderer.material.color = c;
            }
        }

    }


}
