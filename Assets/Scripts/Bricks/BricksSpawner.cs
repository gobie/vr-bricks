﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BricksSpawner : MonoBehaviour
{

    public GameObject BricksPrefab;

    public float BrickTransparencyHit = 0.5f;

    public string parentToAtStart = "BricksSlot";

	private GameObject bricksInstance;

    void Start ()
    {
        Debug.Assert (BricksPrefab != null, "Need Bricks prefab for Bricks spawner");
    }
    
    	void OnEnable ()
    	{
    		GameManagerInterface.OnGameStart += SpawnBricks;
    	}
    
    	void OnDisable ()
    	{
    		GameManagerInterface.OnGameStart -= SpawnBricks;
    	}
    
    void SpawnBricks ()
    {
    
		if (bricksInstance != null) {
			DestroyImmediate (bricksInstance);
			bricksInstance = null;
		}

		GameObject bricksParent = GameObject.FindGameObjectWithTag (parentToAtStart);
    
        bricksInstance = Instantiate (BricksPrefab, bricksParent.transform.position, bricksParent.transform.rotation);
        bricksInstance.transform.parent = bricksParent.transform;
    
        BrickSetController brickSetController = BricksPrefab.GetComponent<BrickSetController> ();
        Debug.LogWarning ("brickSetController " + brickSetController);
        BrickControl[] brickControls = BricksPrefab.GetComponentsInChildren<BrickControl> ();
        Debug.LogWarning ("brickControls.length " + brickControls.Length);
        brickSetController.SetNumBricks (brickControls.Length);
        foreach (BrickControl brickControl in brickControls) {
            brickControl.SetBrickTransparencyWhenHit (BrickTransparencyHit);
            brickControl.SetBrickSetController (brickSetController);
        }
    
    }
}
