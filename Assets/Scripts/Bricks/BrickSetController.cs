﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickSetController : MonoBehaviour {
	public int numBricksActive;

	public static BrickSetController instance = null;


	void Awake()
	{
		if (instance == null)
			instance = this;

		else if (instance != this)
			Destroy(gameObject);

		DontDestroyOnLoad(gameObject);
	}




	// Use this for initialization
	void Start () {
		Debug.Log("hp " + numBricksActive);
	}

	public void DecreaseNumBricks() {
		numBricksActive = numBricksActive - 1;
		Debug.LogWarning ("numBricks " + numBricksActive);
		if (numBricksActive <= 0) {
			Object.FindObjectOfType<GameManagerInterface> ().StopGame ();
		}
		HealthControl.instance.ChangeHealth(numBricksActive);

	}

	public void SetNumBricks(int n) {
		numBricksActive = n;
		
	}

	// Update is called once per frame
	/*
	void Update () {
			
	}
	*/
}
