﻿using UnityEngine;
using System.Collections;

public class GameManager
{

    public bool GameIsRunning { private set; get; }

    public float GameTime { private set; get; }

    public float GameScore { private set; get; }

    public void StartGame ()
    {
        if (GameIsRunning)
        {
            throw new System.Exception ("Cannot start game when game is already running");
        } else {
            GameTime = 0;
            GameScore = 0;
            GameIsRunning = true;
        }
    }

    public void StopGame ()
    {
        if (GameIsRunning) {
            GameIsRunning = false;
        } else {
            throw new System.Exception ("Cannot stop game when game is not running");
        }
    }

    public void IncrementCurrentTime (float time)
    {
        if (time <= 0) {
            throw new System.Exception ("Cannot add 0 or less time to clock");
        } else if (!GameIsRunning) {
            throw new System.Exception ("Cannot add time when game is not running");
        } else {
            GameTime += time;
        }
    }

    public void AddScore (float scoreToAdd)
    {
        if (!GameIsRunning) {
            throw new System.Exception ("Cannot add score when game is not running");
        } else {
            GameScore += scoreToAdd;
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartGame();
        }
    }
}