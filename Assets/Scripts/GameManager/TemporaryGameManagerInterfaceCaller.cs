﻿using UnityEngine;
using System.Collections;

namespace DragonNest {
    public class TemporaryGameManagerInterfaceCaller : MonoBehaviour {

        public float StartGameDelay = 4.0f;
        public float StopGameDelay = 10f;

        public float[] AddScoreAtTimes;

        // Use this for initialization
        void Start() {
            Invoke("StartGame", StartGameDelay);

            foreach(float time in AddScoreAtTimes) {
                Invoke("AddScore", time);
            }
            if(StopGameDelay > 0) {
                Invoke("StopGame", StopGameDelay);
            }

        }

        void AddScore() {
            (Object.FindObjectOfType<GameManagerInterface>() as GameManagerInterface).AddScore(200f);
        }

        void StartGame() {
            (Object.FindObjectOfType<GameManagerInterface>() as GameManagerInterface).StartGame();
        }


        void StopGame() {
            (Object.FindObjectOfType<GameManagerInterface>() as GameManagerInterface).StopGame();
        }
    }
}