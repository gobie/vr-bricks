﻿using UnityEngine;
using System.Collections;

public class GameManagerInterface : MonoBehaviour
{

    public delegate void GameStarted ();

    public static event GameStarted OnGameStart;

    public delegate void GameStopped (float finalTime, float finalScore);

    public static event GameStopped OnGameStop;

    public delegate void ScoreChanged (float newScore);

    public static event ScoreChanged OnScoreChange;

    private GameManager _GameManager;

    public bool GameIsRunning {
        get {
            return _GameManager.GameIsRunning;
        }
    }

    void Awake ()
    {
        _GameManager = new GameManager ();

		if (Display.displays.Length > 1)
			Display.displays [1].Activate ();
    }

    void Update ()
    {
        if (_GameManager.GameIsRunning) {
            _GameManager.IncrementCurrentTime (Time.deltaTime);
        }
    }

    public void StartGame ()
    {
        _GameManager.StartGame ();
        if (OnGameStart != null) {
            OnGameStart ();
        }
    }

    public void StopGame ()
    {
        _GameManager.StopGame ();
        float finalTime = _GameManager.GameTime;
        float finalScore = _GameManager.GameScore;
        if (OnGameStop != null) {
            OnGameStop (finalTime, finalScore);
        }
    }

    public void AddScore (float score)
    {
        _GameManager.AddScore (score);
        float newScore = _GameManager.GameScore;
        if (OnScoreChange != null) {
            OnScoreChange (newScore);
        }
    }

    public float CurrentGameTime ()
    {
        return _GameManager.GameTime;
    }

}