﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthControl : MonoBehaviour {

	public List<Image> health;
	public int hp;
	public static HealthControl instance = null;

	void Awake()
	{
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy(gameObject);

		DontDestroyOnLoad(gameObject);
	}



	public void ChangeHealth(int hp)
	{
		
		//hp = 0;
		health[hp-1].GetComponent<Image>().color = Color.white;
		Debug.Log("health changed");
	}

	// Use this for initialization


	// Use this for initialization
	void Start () {
		if (BrickSetController.instance != null)
			hp = BrickSetController.instance.numBricksActive;
	}
	
	// Update is called once per frame
	void Update () {
	}




}
