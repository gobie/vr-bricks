﻿using UnityEngine;
using System.Collections;

namespace DragonNest {

    public class UIRunningScore : MonoBehaviour {

        public bool _UseRegularScore = false;
        private string _TextToDisplay;
        private TextMesh _ScoreTextMesh;
        private bool _UIShouldBeActive = true;
        private GameManagerInterface _CurrentInterface;

        // Use this for initialization
        void Awake() {
            _ScoreTextMesh = GetComponent<TextMesh>();

            if ( !_UseRegularScore ) {
                _CurrentInterface = Object.FindObjectOfType<GameManagerInterface>();
            }
        }

        void OnEnable() {
            if ( _UseRegularScore ) {
                GameManagerInterface.OnScoreChange += UpdateScore;
            }
            GameManagerInterface.OnGameStop += GameEnded;
        }

        void OnDisable() {
            if ( _UseRegularScore ) {
                GameManagerInterface.OnScoreChange -= UpdateScore;
            }
            GameManagerInterface.OnGameStop -= GameEnded;

        }

        void GameEnded(float finalTime, float finalScore) {
            string gameEnded = "Game Over";
            if ( _UseRegularScore ) {
                gameEnded += "\nFinal Score: " + finalScore.ToString("F0") + "p";
            } else {
                gameEnded += "\nFinal Time: " + TextBasedOnRunningGameTime();
            }
            _ScoreTextMesh.text = gameEnded;
            _UIShouldBeActive = false;
        }

        void UpdateScore(float NewScore) {
            _TextToDisplay = NewScore.ToString("F0") + "p";
        }

        void Update() {
            if ( _UIShouldBeActive ) {
                if ( !_UseRegularScore ) {
                    _TextToDisplay = TextBasedOnRunningGameTime();
                }
                _ScoreTextMesh.text = _TextToDisplay;
            }

        }

        string TextBasedOnRunningGameTime() {
            float currentTime = _CurrentInterface.CurrentGameTime();
            float minutes = ((int)currentTime) / 60;
            float seconds = currentTime - minutes * 60f;
            string formattedTime = seconds.ToString("0.") + "s";
            if ( minutes > 0 ) {
                formattedTime = minutes.ToString("0.") + ":" + formattedTime;
            }
            return formattedTime;
        }
    }
}