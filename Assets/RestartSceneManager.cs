﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartSceneManager : MonoBehaviour
{

    public KeyCode RestartKey;

    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKeyDown (RestartKey)) {
            //UnityEngine.SceneManagement.SceneManager.LoadScene (UnityEngine.SceneManagement.SceneManager.GetActiveScene ().name);
			Object.FindObjectOfType<GameManagerInterface> ().StartGame ();
		}	
    }
}
