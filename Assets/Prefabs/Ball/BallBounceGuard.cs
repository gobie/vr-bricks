﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//This script makes sure that the ball does not get stuck and LOCKS itself in one axis.
public class BallBounceGuard : MonoBehaviour
{
    [Tooltip ("If the balls normalised direction has an axis above this value, then shift up its direction a bit in order to avoid it being locked in an axis")]
    public float Threshold = 0.95f;
    [Tooltip ("If deciding to shift, shift towards a point that is in the range of (-this, this) around origo.")]
    public float RandomRange = 0.5f;
    private Rigidbody ThisRB;

    void Start ()
    {
        ThisRB = GetComponent<Rigidbody> ();
    }

    void OnCollisionExit (Collision col)
    {
        Vector3 velocity = ThisRB.velocity;
//        Debug.Log ("VELOC BEFORE : " + velocity);
        float zeroRange = 0.2f;
        if (Mathf.Abs (velocity.x) < 0.05) {
            velocity.x = Random.Range (0, 1.0f) > 0.5f ? zeroRange : -zeroRange;
        }

        if (Mathf.Abs (velocity.y) < 0.05) {
            velocity.y = Random.Range (0, 1.0f) > 0.5f ? zeroRange : -zeroRange;
        }


        if (Mathf.Abs (velocity.z) < 0.05) {
            velocity.z = Random.Range (0, 1.0f) > 0.5f ? zeroRange : -zeroRange;
        }
        velocity.Normalize ();
        float RandomRange = 0.05f;
        velocity.x = Random.Range (velocity.x * (1 - RandomRange), velocity.x * (1 + RandomRange));
        velocity.y = Random.Range (velocity.y * (1 - RandomRange), velocity.y * (1 + RandomRange));
        velocity.z = Random.Range (velocity.z * (1 - RandomRange), velocity.z * (1 + RandomRange));

        velocity.Normalize ();
        velocity *= 5;
        ThisRB.velocity = velocity;
//        Debug.Log ("VELOC AFTER: " + velocity);
      
    }
}
