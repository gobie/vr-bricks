﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomVelocityAtStart : MonoBehaviour
{
    public float ballSpeed = 1.0f;
    // Use this for initialization
    void Start ()
    {
        Vector3 velocityVector = new Vector3 (Random.Range (-0.5f, 0.5f), Random.Range (0, 1.0f), Random.Range (-0.5f, 0.5f));
        velocityVector.Normalize ();
        GetComponent<Rigidbody> ().velocity = velocityVector * ballSpeed;

    }
	

}
