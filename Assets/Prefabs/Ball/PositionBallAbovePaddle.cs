﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionBallAbovePaddle : MonoBehaviour
{

    public Transform paddleTransform;
    private Transform thisTransform;
    private GameManagerInterface GMI;

    void Awake ()
    {
        GMI = Object.FindObjectOfType <GameManagerInterface> ();
        thisTransform = this.transform;
    }


    void Update ()
    {
        if (!GMI.GameIsRunning) {
            thisTransform.position = paddleTransform.position + new Vector3 (0, 1.0f, 0);
        }
    }
}
