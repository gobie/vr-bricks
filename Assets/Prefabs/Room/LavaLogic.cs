﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaLogic : MonoBehaviour
{

	public int numLives = 4;
    public GameObject LavaExplosionVFX;
    public AudioClip LavaExplosionSFX;

    void OnCollisionEnter (Collision col)
    {
        if (col.collider.tag == "Ball") {
            GameObject lavaExplosion = Instantiate (LavaExplosionVFX, col.transform.position, Quaternion.identity);
            Destroy (lavaExplosion, 3.0f);
            AudioSource.PlayClipAtPoint (LavaExplosionSFX, col.transform.position);
            //Object.FindObjectOfType<GameManagerInterface> ().StopGame ();
			BallSpawner.instance.DecreaseBalls ();
            Destroy (col.gameObject);
        }
    }
}
