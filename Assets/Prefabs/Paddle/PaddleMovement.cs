﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Rigidbody))]
public class PaddleMovement : MonoBehaviour
{

    public KeyCode MovePaddleLeft;
    public KeyCode MovePaddleRight;
    //public KeyCode StartGameButton;


    public float PaddleMovementSpeed;
    private  Rigidbody ThisRB;
    private Transform ThisT;
    public Vector3 MovementVector;



    void Start ()
    {
        ThisT = this.transform;
        ThisRB = GetComponent<Rigidbody> ();
    }
	
    // Update is called once per frame
    void Update ()
    {

        /*
        bool UserWantsToStartGame = Input.GetKeyDown (StartGameButton);
		if (UserWantsToStartGame) {
            GameManagerInterface GMI = Object.FindObjectOfType<GameManagerInterface> ();
            if (!GMI.GameIsRunning) {
                GMI.StartGame ();
            }
        }
        */
        bool Right = Input.GetKey(KeyCode.RightArrow); 
        bool Left = Input.GetKey(KeyCode.LeftArrow);

        if (Right && !Left) {
            ThisRB.MovePosition (ThisT.position + Time.deltaTime * MovementVector * (1) * PaddleMovementSpeed);
        } else if (Left && !Right) {
            ThisRB.MovePosition (ThisT.position + Time.deltaTime * MovementVector * (-1) * PaddleMovementSpeed);
        }
    }
}
